import requests
from bs4 import BeautifulSoup
import time
import random

from globaldefs import *

def getRandomWait():
    return float(random.randint(5, 100)) / 10.0

def formatUrl(url):
        if url[0:4] == 'http' or url[0:4] == 'imdb' or url[0:6] == 'm.imdb':
            return url
        else :
            decomposed = list(filter(lambda sub: sub != '' or sub != ' ', url.split('/')))
            return '/'.join([BASE_URL] + decomposed)

def loadMoreNoMo(url, bs, header = {}):
    loadMore = bs.find('div', { 'class' : 'load-more-data' })
    if not loadMore or not loadMore.get('data-key', None):
        return (True, bs, 200)
    ajaxUrl = loadMore.get('data-ajaxurl', url + '/_ajax')
    if not ajaxUrl:
        return (True, bs, 200)
    url = formatUrl(ajaxUrl)
    dataKey = loadMore['data-key']
    if '?' in url:
        url = url + '&'
    else:
        url = url + '?'
    url = url + 'paginationKey=' + dataKey
    time.sleep(getRandomWait())
    header.update(DEFAULT_HEADER)
    print('visiting: %s' % url)
    response = requests.get(url, headers=header)
    bs = BeautifulSoup(response.text, 'html.parser')
    return (False, bs, response.status_code)

def makeSoupFromUrl(url, header = {}):
    header.update(DEFAULT_HEADER)
    time.sleep(getRandomWait())
    print('visiting: %s' % url)
    page = requests.get(url, headers=header)
    soup = BeautifulSoup(page.content, 'html.parser')
    return (soup, page.status_code)
