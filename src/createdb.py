import pymongo

DB_NAME = 'imdb'
USERS_COLLECTION = 'users'
MOVIES_COLLECTION = 'movies'
REVIEWS_COLLECTION = 'reviews'

dbClient = pymongo.MongoClient('mongodb://localhost:27017/')
mydb = dbClient[DB_NAME]


usersCollection = mydb[USERS_COLLECTION]
moviesCollection = mydb[MOVIES_COLLECTION]
reviewsCollection = mydb[REVIEWS_COLLECTION]

usersDict = {
    'id' : 'placeholder',
    'visited' : True
}

moviesDict = {
    'id' : 'placeholder',
    'title' : 'placeholder',
    'year' : 2021,
    'visited' : True
}

reviesDict = {
    'id' : 'placeholder',
    'movieId' : 'placeholder',
    'title' : 'Interstellar',
    'year' : '2014',
    'userId' : 'me',
    'rating' : 10
}

usersCollection.insert_one(usersDict)
moviesCollection.insert_one(moviesDict)
reviewsCollection.insert_one(reviesDict)

dblist = dbClient.list_database_names()
if DB_NAME in dblist:
    print('Database successfully created.')
    print('+------------------------------+')
    print('|       !!!IMPORTANT!!!        |')
    print('+------------------------------+')
    print('| MAKE SURE TO CLEAN UP THE DB |')
    print('+------------------------------+')
else:
    raise RuntimeError('Could not create a database %s' % DB_NAME)
