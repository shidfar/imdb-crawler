import pymongo

from globaldefs import *

def getDbClient():
    dbClient = pymongo.MongoClient('mongodb://localhost:27017/')
    mydb = dbClient[DB_NAME]

    usersCollection = mydb[USERS_COLLECTION]
    moviesCollection = mydb[MOVIES_COLLECTION]
    reviewsCollection = mydb[REVIEWS_COLLECTION]

    def addNewMovie(mid, title, year, visited = False):
        insertOneResult = moviesCollection.insert_one({
            'id' : mid,
            'title' : title,
            'year' : year,
            'visited' : visited
        })
        return insertOneResult.acknowledged

    def addNewUser(uid, visited = False):
        insertOneResult = usersCollection.insert_one({ 'id': uid, 'visited' : visited })
        return insertOneResult.acknowledged

    def addNewReview(rid, mid, uid, title, year, rating):
        insertOneResult = reviewsCollection.insert_one({
            'id' : rid,
            'movieId': mid,
            'userId': uid,
            'title' : title,
            'year' : year,
            'rating' : rating
        })
        return insertOneResult.acknowledged

    def getUnvisitedMovies():
        crsr = moviesCollection.aggregate([
            { '$match': { 'visited' : False } },
            { '$sample': { 'size': 10 } }
        ])
        return list(crsr)

    def getUnvisitedUsers():
        crsr = usersCollection.aggregate([
            { '$match': { 'visited' : False } },
            { '$sample': { 'size': 10 } }
        ])
        return list(crsr)

    def markVisitedMovie(mid, visited = True):
        query = { 'id': mid }
        newvalues = { '$set': { 'visited': visited } }
        updateResult = moviesCollection.update_one(query, newvalues)
        return updateResult.acknowledged

    def markVisitedUser(uid, visited = True):
        query = { 'id': uid }
        newvalues = { '$set': { 'visited': visited } }
        updateResult = usersCollection.update_one(query, newvalues)
        return updateResult.acknowledged

    def movieExists(mid):
        query = { 'id': mid }
        movies = moviesCollection.find(query)
        return (len(list(movies)) > 0)

    def userExists(uid):
        query = { 'id': uid }
        users = usersCollection.find(query)
        return (len(list(users)) > 0)

    def reviewExists(rid):
        query = { 'id': rid }
        reviews = reviewsCollection.find(query)
        return (len(list(reviews)) > 0)

    return new('IMDb', {
        'addNewMovie' : addNewMovie,
        'addNewUser' : addNewUser,
        'addNewReview' : addNewReview,
        'getUnvisitedMovies' : getUnvisitedMovies,
        'getUnvisitedUsers' : getUnvisitedUsers,
        'markVisitedMovie' : markVisitedMovie,
        'markVisitedUser' : markVisitedUser,
        'movieExists' : movieExists,
        'userExists' : userExists,
        'reviewExists' : reviewExists,
    })
