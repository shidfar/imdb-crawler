import random
import requests
import time
from bs4 import BeautifulSoup

import crawlerutils
from globaldefs import *

def getDiversionMaker():

    def formatUrl(url):
        if url[0:4] == 'http' or url[0:4] == 'imdb' or url[0:6] == 'm.imdb':
            return url
        else :
            decomposed = list(filter(lambda sub: sub != '' or sub != ' ', url.split('/')))
            return '/'.join([BASE_URL] + decomposed)

    def listAllHyperlinks(soup):
        aTags = soup.find_all('a')
        links = []
        if len(aTags) < 1:
            return []
        for aTag in aTags:
            if aTag.get('href'):
                links.append(formatUrl(aTag.get('href')))
        return links


    def startDiversion(depth = 3):
        print('----------- WAZAAAAAP! ----------')
        links = [BASE_URL]
        for i in range(depth):
            randWait = crawlerutils.getRandomWait()
            randIdx = random.randint(0, len(links) - 1)
            print(f'Randomly waiting for {randWait:.2f} seconds')
            print('Randomly visiting %dth link: %s' % (randIdx, links[randIdx]))
            print('\n')
            time.sleep(randWait)
            (soup, statusCode) = crawlerutils.makeSoupFromUrl(links[randIdx])
            if statusCode != 200:
                return (statusCode, 'Diversion failed.')
            nextLinks = listAllHyperlinks(soup)
            if len(nextLinks) > 0:
                print('got %d new links\n' % len(nextLinks))
                links = nextLinks
        print('------ Diversion ended bye ----\n')
        return (200, 'success')
        
    return new('Divergent', {
        'startDiversion' : startDiversion
    })
