DB_NAME = 'imdb'
USERS_COLLECTION = 'users'
MOVIES_COLLECTION = 'movies'
REVIEWS_COLLECTION = 'reviews'

BASE_URL='https://www.imdb.com'
MOVIE_SUFFIX='title'
REVIEWS_SUFFIX='reviews'
USER_SUFFIX='user'
DEFAULT_HEADER = {
  'Accept' : '*/*',
  'Content-Type': 'text/plain;charset=UTF-8',
  'Cache-Control': 'max-age=0',  
  'Origin': 'https://www.imdb.com',
  'Referer': 'https://www.imdb.com/',
  'Accept-Language': 'en-us,en;',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.3 Safari/605.1.15',
  'Connection': 'keep-alive'
  }

TOP_250 = 'chart/top/?ref_=nv_mv_250'
PARSER = 'html.parser'

# https://www.imdb.com/title/tt7888964/reviews/_ajax?paginationKey=g4wp7drkqa2dezqf7sxxrnburdt4mbrh
# https://www.imdb.com/user/ur16161013/reviews/_ajax?sort=submissionDate&dir=desc&ratingFilter=0&
#                                                                   paginationKey=g4w6jbbnryztmzic76

def new(name, data):
  return type(name, (object,), data)



def makeFibonacci():
  def plus(a, b):
    return (b, a + b)
  x1 = 5
  x2 = 8
  sequence = [5, 8]
  for i in range(11):
    (x1, x2) = plus(x1, x2)
    sequence.append(x2)
  return sequence
