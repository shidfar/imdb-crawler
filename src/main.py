import sys, argparse, signal, os
import requests
import asyncio, aiohttp
import calendar
import time
import pymongo
from urllib.parse import urljoin
from bs4 import BeautifulSoup
from functools import reduce
from datetime import datetime

import dbclient as db
import moviecrawler as mc
import usercrawler as uc
import diversionmaker
from globaldefs import *

def main():
    imdb = db.getDbClient()
    movieCrawler = mc.getMovieCrawler(imdb)
    userCrawler = uc.getUserCrawler(imdb)
    unvisitedMovies = imdb.getUnvisitedMovies()
    unvisitedUsers = imdb.getUnvisitedUsers()
    divirgent = diversionmaker.getDiversionMaker()

    def doRamndomstuff(depth = 3):
        now = datetime.now()
        datetimeStr = now.strftime("%d/%m/%Y %H:%M:%S")
        print('Sleeping for 1 minute from: %s' % datetimeStr)
        print('\n')
        time.sleep(60)
        return divirgent.startDiversion(depth)

    def nextCycle(circle, circleEnd):
        if circle + 1 >= circleEnd:
            return 0
        return circle + 1

    fibonacci = makeFibonacci()
    circle = 0
    circleEnd = len(fibonacci)
    doRamndomstuff(11)
    if len(unvisitedMovies) == 0:
        movieCrawler.fillMoviesFromTop250()
        unvisitedMovies = imdb.getUnvisitedMovies()
    while len(unvisitedMovies) > 0 or len(unvisitedUsers) > 0:
        (status, message) = userCrawler.crawlUnvisitedUsers()
        if status != 200:
            print('Crawling users failed with: %d, %s' % (status, message))
            while status != 200:
                circle = nextCycle(circle, circleEnd)
                (status, message) = doRamndomstuff(fibonacci[circle])
                print('Diversion result: %d, %s' % (status, message))
        else :
            print('Diversion successful\n')
            circle = 0
        (status, message) = movieCrawler.crawlUnvisitedMovies()
        if status != 200:
            print('Crawling movies failed with: %d, %s' % (status, message))
            while status != 200:
                circle = nextCycle(circle, circleEnd)
                (status, message) = doRamndomstuff(fibonacci[circle])
                print('Diversion result: %d, %s' % (status, message))
        else :
            print('Diversion successful\n')
            circle = 0


def mainLock():
    while True:
        try:
            main()
        except Exception as ex:
            print('-------------\n')
            print('Houston we have a problem\n')
            print(ex)
            print('-------------\n')

if __name__ == '__main__':
    exit(mainLock())
