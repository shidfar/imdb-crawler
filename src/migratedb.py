import pymongo

DB_NAME = 'imdb'
USERS_COLLECTION = 'users'
MOVIES_COLLECTION = 'movies'
REVIEWS_COLLECTION = 'reviews'

dbClient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = dbClient[DB_NAME]

usersCollection = mydb[USERS_COLLECTION]
moviesCollection = mydb[MOVIES_COLLECTION]
reviewsCollection = mydb[REVIEWS_COLLECTION]

moviesSet = set()
usersSet = set()
reviewSet = set()

def main():
    usersArray = []
    warmupUsersFile = open('data/warmup/userIds', 'r', encoding='utf-8')
    userLines = warmupUsersFile.readlines()
    warmupUsersFile.close()
    for userId in userLines:
        # print(userId.encode('utf-8'))
        uid = userId.strip('\n')
        if not uid in usersSet:
            usersSet.add(uid)
            usersArray.append({ 'id': uid, 'visited' : False })
    usersCollection.insert_many(usersArray)

    moviesArray = []
    warmupMoviesFile = open('data/warmup/movieIds', 'r', encoding='utf-8')
    movieLines = warmupMoviesFile.readlines()
    warmupMoviesFile.close()
    for movieLine in movieLines:
        # print(movieLine.encode('utf-8'))
        separatedMovieLine = movieLine.strip('\n').split(';')
        mid = separatedMovieLine[0]
        title = separatedMovieLine[1]
        yearFilter = filter(str.isdigit, separatedMovieLine[2])
        year = ''.join(yearFilter)
        if not mid in moviesSet:
            moviesSet.add(mid)
            moviesArray.append({
                'id' : mid,
                'title' : title,
                'year' : year,
                'visited' : True
            })
    moviesCollection.insert_many(moviesArray)

    reviewsArray = []
    warmupReviewsFile = open('data/warmup/reviewIds', 'r', encoding='utf-8')
    reviewLines = warmupReviewsFile.readlines()
    warmupReviewsFile.close()
    for reviewLine in reviewLines:
        # print(reviewLine.encode('utf-8'))
        separatedReviewLine = reviewLine.strip('\n').split(';')
        rid = separatedReviewLine[0]
        mid = separatedReviewLine[1]
        title = separatedReviewLine[2]
        yearFilter = filter(str.isdigit, separatedReviewLine[3])
        year = ''.join(yearFilter)
        uid = separatedReviewLine[4]
        rating = separatedReviewLine[5]
        if not rid in reviewSet:
            reviewSet.add(rid)
            reviewsArray.append({
                'id' : rid,
                'movieId': mid,
                'userId': uid,
                'title' : title,
                'year' : year,
                'rating' : rating
            })
    reviewsCollection.insert_many(reviewsArray)

if __name__ == '__main__':
    exit(main())
