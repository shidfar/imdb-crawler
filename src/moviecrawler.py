import requests
from bs4 import BeautifulSoup
import time

import crawlerutils
from globaldefs import *

def getMovieCrawler(db):
    def pretify250Row(titleColumn):
        titleColumnAddressTag = titleColumn.find('a')
        movieId = titleColumnAddressTag['href'].split('/')[2]
        *titleRowAsList, = map(lambda word: word.strip(';'),  titleColumn.stripped_strings)
        titleRowAsList.pop(0)
        csvedRow = ';'.join(titleRowAsList)
        return (csvedRow, movieId)

    def getTop250Page():
        url = BASE_URL + '/chart/top/?ref_=nv_mv_250'
        time.sleep(crawlerutils.getRandomWait())
        print('visiting: %s' % url)
        page = requests.get(url, headers=DEFAULT_HEADER)
        bs = BeautifulSoup(page.content, 'html.parser')
        return bs.find('table', { 'data-caller-name': 'chart-top250movie' }).find('tbody').find_all('tr')

    def storeMovieListFromTop250(top250):
        movieList = []
        for movie in top250:
            titleColumn = movie.find('td', { 'class': 'titleColumn' })
            (csvedRow, movieId) = pretify250Row(titleColumn)
            movieDesc = csvedRow.split(';')
            if (not db.movieExists(movieId)):
                db.addNewMovie(movieId, movieDesc[0], movieDesc[1])
            else :
                print('skip %s, %s, %s' % (movieId, movieDesc[0], movieDesc[1]))

    def fillMoviesFromTop250():
        top250 = getTop250Page()
        storeMovieListFromTop250(top250)

    def makeSoupFromUrl(url, header = {}):
        header.update(DEFAULT_HEADER)
        time.sleep(crawlerutils.getRandomWait())
        print('visiting: %s' % url)
        page = requests.get(url, headers=header)
        soup = BeautifulSoup(page.content, 'html.parser')
        return (soup, page.status_code)

    def getListerFromSoup(soup):
        userReviewsLister = soup.find('div', { 'class' : 'lister-list' })
        if not userReviewsLister:
            return None
        reviewItems = userReviewsLister.find_all('div', { 'class' : 'lister-item mode-detail imdb-user-review collapsable' })
        if not reviewItems:
            return None
        return reviewItems

    def fillUsersFromLister(lister):
        counter = 0
        for userReview in lister:
            userATag = userReview.find('span', { 'class' : 'display-name-link' }).find('a')
            uid = userATag['href'].split('/')[2]
            userName = userATag.text
            if not db.userExists(uid):
                counter += db.addNewUser(uid)
        return counter

    def digUsersFromMovie(mid):
        url = '/'.join([BASE_URL, MOVIE_SUFFIX, mid, REVIEWS_SUFFIX])
        (soup, statusCode) = makeSoupFromUrl(url)
        if statusCode != 200:
            return (statusCode, 'failed to get movie page')
        moreNoMo = False
        while not moreNoMo:
            lister = getListerFromSoup(soup)
            if not lister:
                return (404, 'lister not found')
            time.sleep(crawlerutils.getRandomWait())
            usersAdded = fillUsersFromLister(lister)
            print('%d new users added' % usersAdded)
            (moreNoMo, soup, statusCode) = crawlerutils.loadMoreNoMo(url, soup)
            if statusCode != 200:
                return (statusCode, 'failed to load more content')
        return (200, 'success')


    def crawlUnvisitedMovies():
        movies = db.getUnvisitedMovies()
        for movie in movies:
            mid = movie['id']
            (status, message) = digUsersFromMovie(mid)
            if status != 200:
                print('The movie %s failed to crawl with: %d, %s' % (mid, status, message))
                return (status, message)
            else:
                print('Movie %s was successfully crawled. Marking as visited.' % mid)
                db.markVisitedMovie(mid)
        return (200, 'success')

    return new('MovieCrawler', {
        'fillMoviesFromTop250' : fillMoviesFromTop250,
        'crawlUnvisitedMovies' : crawlUnvisitedMovies
    })
