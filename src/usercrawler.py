import requests
import time
from bs4 import BeautifulSoup

import crawlerutils
from globaldefs import *

def getUserCrawler(db):
    def makeSoupFromUrl(url, header = {}):
        header.update(DEFAULT_HEADER)
        time.sleep(crawlerutils.getRandomWait())
        print('visiting: %s' % url)
        page = requests.get(url, headers=header)
        soup = BeautifulSoup(page.content, 'html.parser')
        return (soup, page.status_code)

    def getListerFromSoup(soup):
        movieReviewsLister = soup.find('div', { 'class' : 'lister-list' })
        if not movieReviewsLister:
            return None
        movieReviews = movieReviewsLister.find_all('div', { 'class' : 'lister-item mode-detail imdb-user-review with-image collapsable' })
        if not movieReviews:
            return None
        return movieReviews

    def fillMoviesAndReviewsFromLister(lister, uid):
        movieCounter = 0
        reviewCounter = 0
        for movieReview in lister:
            rid = movieReview.get('data-review-id')
            reviewHeader = movieReview.find('div', { 'class' : 'lister-item-header' })
            ratingSpan = movieReview.find('span', { 'class' : 'rating-other-user-rating' })
            if not rid or db.reviewExists(rid) or not ratingSpan or not reviewHeader:
                continue
            rating = ratingSpan.find_all('span')[0].text
            mid = reviewHeader.find('a')['href'].split('/')[2]
            *nameAndYear, = map(lambda word: word.strip(';'),  reviewHeader.stripped_strings)
            csvedNameRow = ';'.join(nameAndYear)
            uncved = csvedNameRow.split(';')
            title = uncved[0]
            year = uncved[1]
            if not db.reviewExists(rid):
                reviewCounter += db.addNewReview(rid, mid, uid, title, year, rating)
            if not db.movieExists(mid):
                movieCounter += db.addNewMovie(mid, title, year)
        return (movieCounter, reviewCounter)

    def digReviewsAndMoviesFromUser(uid):
        url = '/'.join([BASE_URL, USER_SUFFIX, uid, REVIEWS_SUFFIX])
        (soup, statusCode) = makeSoupFromUrl(url)
        if statusCode != 200:
            return (statusCode, 'failed to get user reviews page')
        moreNoMo = False
        while not moreNoMo:
            lister = getListerFromSoup(soup)
            if not lister:
                return (404, 'lister not found')
            time.sleep(crawlerutils.getRandomWait())
            (moviesAdded, reviewsAdded) = fillMoviesAndReviewsFromLister(lister, uid)
            print('%s new movies and %s new reviews added' % (moviesAdded, reviewsAdded))
            (moreNoMo, soup, statusCode) = crawlerutils.loadMoreNoMo(url, soup)
            if statusCode != 200:
                return (statusCode, 'failed to load more content')
        return (200, 'success')

    def crawlUnvisitedUsers():
        users = db.getUnvisitedUsers()
        for user in users:
            uid = user['id']
            (status, message) = digReviewsAndMoviesFromUser(uid)
            if status != 200:
                print('The user %s faild to crawl with: %d, %s' % (uid, status, message))
                return (status, message)
            else:
                print('User %s was successfully crawled. Marking as visited.' % uid)
                db.markVisitedUser(uid)
        return (200, 'success')

    return new('UsersCrawler', {
        'crawlUnvisitedUsers' : crawlUnvisitedUsers
    })
